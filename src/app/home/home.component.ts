import { BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit ,Output,EventEmitter} from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }
public onToggleSidenav = () => {
  this.sidenavToggle.emit();
}

  
}
