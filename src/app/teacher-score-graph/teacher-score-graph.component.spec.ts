import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherScoreGraphComponent } from './teacher-score-graph.component';

describe('TeacherScoreGraphComponent', () => {
  let component: TeacherScoreGraphComponent;
  let fixture: ComponentFixture<TeacherScoreGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherScoreGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherScoreGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
