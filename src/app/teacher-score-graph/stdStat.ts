export class classstats {
    id: number;
    stdId: number;
    name: String;
    score: number;
    minscore:number;
    maxscore: number;
    average: number;

    constructor(score,minscore,maxscore,average){
        this.score = score;
        this.minscore = minscore;
        this.maxscore = maxscore;
        this.average = average;
    }
}