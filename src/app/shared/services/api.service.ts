import { Observable } from 'rxjs';
import {throwError as observableThrowError} from 'rxjs';

import {catchError} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
// shall I use HttpClient
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
// tslint:disable-next-line:import-blacklist


import { Router } from '@angular/router';

@Injectable()
export class ApiService {
  private jsonHeader = 'application/json';
  // private uploadHeader: string = '';

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
  }

  /**
   * this function set header after sending
   */
  setHeaders(type: string): HttpHeaders {
    const headersConfig = {
      'Content-Type': type,
      'X-Requested-With': 'XMLHttpRequest',
      'Accept': 'application/json'
    };
    //if (this.jwtService.getToken()) {
     // headersConfig['Authorization'] = `Token ${this.jwtService.getToken()}`;
   // }
    return new HttpHeaders(headersConfig);
  }

  //getHeaders(): HttpHeaders {
    //const headersConfig = {
     // 'Authorization': `Token ${this.jwtService.getToken()}`,
    //};
   // return new HttpHeaders(headersConfig);
  //}


  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(
      `http://localhost:3000`+path,
      { headers: this.setHeaders(this.jsonHeader), params: params }
    );
  }

}

