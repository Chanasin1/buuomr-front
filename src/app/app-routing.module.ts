import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { CourseComponent } from './course/course.component';
import { ScoreComponent } from './score/score.component';
import { MyScoreComponent } from './my-score/my-score.component';
import { TeacherScoreComponent } from './teacher-score/teacher-score.component';
import { DownloadScoreComponent } from './download-score/download-score.component';
import { RootNavComponent } from './root-nav/root-nav.component';
import { ReportComponent } from './report/report.component';
import { MyScoreDetailComponent } from './my-score-detail/my-score-detail.component';
import { MyScoreGraphComponent } from './my-score-graph/my-score-graph.component';
import { TeacherScoreDetailComponent } from './teacher-score-detail/teacher-score-detail.component';
import { TeacherScoreGraphComponent } from './teacher-score-graph/teacher-score-graph.component';
import { DetailComponent } from './my-score/detail/detail.component';
import { TeacherExamDetailComponent } from './teacher-exam-detail/teacher-exam-detail.component';
import { HomeTeacherComponent } from './home-teacher/home-teacher.component';
import { TestGraphComponent } from './test-graph/test-graph.component';

const routes: Routes = [
  { path: '',redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'course', component: CourseComponent },
  { path: 'Score', component: ScoreComponent },
  { path: 'myScore', component: MyScoreComponent },
  { path: 'teacherScore', component: TeacherScoreComponent },
  { path: 'download', component: DownloadScoreComponent },
  { path: 'root', component: RootNavComponent },
  { path: 'report',component: ReportComponent},
  { path: 'myScoreDetail', component: MyScoreDetailComponent },
  { path: 'myScoreGraph', component: MyScoreGraphComponent },
  { path: 'teacherScoreDetail', component: TeacherScoreDetailComponent },
  { path: 'teacherScoreGraph', component: TeacherScoreGraphComponent },
  { path: 'detail', component: DetailComponent },
  { path: 'teacherExamDetail', component: TeacherExamDetailComponent },
  { path: 'home-teacher', component: HomeTeacherComponent },
  { path: 'test', component: TestGraphComponent },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
