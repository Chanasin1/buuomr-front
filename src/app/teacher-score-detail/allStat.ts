export class allStat {
    id: number;
    stdId: number;
    name: string;
    lastname: string;
    group: string;
    score: string

    constructor(id,stdId,name,lastname,group,score){
        this.id = id;
        this.stdId = stdId;
        this.name = name;
        this.lastname = lastname;
        this.group = group;
        this.score = score;
    }
}