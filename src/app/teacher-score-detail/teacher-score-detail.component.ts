import { Component, OnInit } from '@angular/core';
import { graphService } from '../my-score-graph/graphService'
import { allStat } from './allStat';
import {MatTableDataSource} from '@angular/material/table';
import { DataSource } from '@angular/cdk/table';


export interface datastat {
  id: number;
  stdId: number;
  name: string;
  lastname: string;
  group: string;
  score: string
}



@Component({
  selector: 'app-teacher-score-detail',
  templateUrl: './teacher-score-detail.component.html',
  styleUrls: ['./teacher-score-detail.component.scss']
})
export class TeacherScoreDetailComponent implements OnInit {
  

  
  displayedColumns: string[] = ['id', 'stdId', 'name', 'lastname','group','score'];
  user : allStat[] = [];
  dataSource :datastat[]=[];
  datasource = new MatTableDataSource(this.dataSource);



  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.datasource.filter = filterValue.trim().toLowerCase();
  }
  

  constructor(
    private graphservice: graphService,
  ) { }

  

  ngOnInit(): void {
    this.graphservice.getAllScore().subscribe((response) => {
      this.user = response
      this.dataSource = response
      this.datasource = new MatTableDataSource(response)
      console.log(this.user)
      console.log(this.dataSource)
      console.log(this.datasource)
  })
  }

}
