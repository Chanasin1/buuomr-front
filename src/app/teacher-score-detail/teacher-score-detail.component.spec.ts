import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherScoreDetailComponent } from './teacher-score-detail.component';

describe('TeacherScoreDetailComponent', () => {
  let component: TeacherScoreDetailComponent;
  let fixture: ComponentFixture<TeacherScoreDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherScoreDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherScoreDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
