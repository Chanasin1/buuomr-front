import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyScoreGraphComponent } from './my-score-graph.component';

describe('MyScoreGraphComponent', () => {
  let component: MyScoreGraphComponent;
  let fixture: ComponentFixture<MyScoreGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyScoreGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyScoreGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
