import { Component, OnInit } from "@angular/core";
import * as Highcharts from "highcharts";
import * as d3 from "d3";
import { graphService } from "./graphService";
import { stdStat } from "./stdStat";

@Component({
  selector: "app-my-score-graph",
  templateUrl: "./my-score-graph.component.html",
  styleUrls: ["./my-score-graph.component.scss"],
})
export class MyScoreGraphComponent implements OnInit {
  user: stdStat[] = [];
  allScore: any;

  typeChart: any;
  dataChart: any;
  optionsChart: any;

  dataHisChart: any;
  optionsHisChart: any;
  bin: any;

  constructor(private graphservice: graphService) {}

  ngOnInit(): void {
    this.graphservice.getStdStat().subscribe((response) => {
      this.user = response;
      console.log(this.user);
    });

    this.graphservice.getAllScore().subscribe((response) => {
      this.allScore = response;
      console.log(this.allScore);
      let score = [];
      let name = [];
      let bg = [];
      console.log(this.allScore);
      console.log(this.user[0]);
      for (var i = 0; i < this.allScore.length; i++) {
        score.push(parseInt(this.allScore[i].score));
        name.push(this.allScore[i].name);
        if (this.allScore[i].stdId == this.user[0].stdId) {
          bg.push("orange");
        } else if (parseInt(this.allScore[i].score) < 10) {
          bg.push("#EC5A40");
        } else if (parseInt(this.allScore[i].score) >= 10) {
          bg.push("#69b3a2");
        }
      }
      this.typeChart = "bar";
      this.dataChart = {
        labels: name,
        datasets: [
          {
            label: "none",
            data: score,
            backgroundColor: bg,
          },
        ],
      };
      this.optionsChart = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false,
        },
        scales: {
          xAxes: [
            {
              categoryPercentage: 1.0,
              barPercentage: 1.0,
            },
          ],
        },
      };
    });

    if (this.bin) {
      this.getHisGraph();
    }
  }

  getHisGraph() {
    let scoreHis = [];
    let nameLength = [];
    let bgHis = [];
    let lengthArr = Math.floor(this.user[0].maxscore / this.bin);
    let i_num = 0;
    while (i_num <= this.user[0].maxscore) {
      if (i_num + lengthArr > this.user[0].maxscore) {
        if (i_num == this.user[0].maxscore) {
          nameLength.push("" + this.user[0].maxscore);
        } else {
          nameLength.push(i_num + "-" + this.user[0].maxscore);
        }
      } else {
        nameLength.push(i_num + "-" + (i_num + lengthArr));
      }
      i_num += lengthArr + 1;
    }
    console.log(nameLength);
    for (var k = 0; k < nameLength.length; k++) {
      scoreHis.push(0);
      bgHis.push("Cornsilk");
    }
    for (var i = 0; i < this.allScore.length; i++) {
      for (var j = 0; j < nameLength.length; j++) {
        var num = nameLength[j].split("-");
        if (
          parseInt(this.allScore[i].score) >= parseInt(num[0]) &&
          parseInt(this.allScore[i].score) <= parseInt(num[1])
        ) {
          scoreHis[j] = scoreHis[j] + 1;
        }
      }
    }
    for (var a = 0; a < nameLength.length; a++) {
      var num = nameLength[a].split("-");
      if (
        this.user[0].score >= parseInt(num[0]) &&
        this.user[0].score <= parseInt(num[1])
      ) {
        bgHis[a] = "Aqua";
      }
    }
    this.dataHisChart = {
      labels: nameLength,
      datasets: [
        {
          label: "จำนวนนักศึกษา",
          data: scoreHis,
          backgroundColor: bgHis,
        },
      ],
    };
    this.optionsHisChart = {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            categoryPercentage: 1.0,
            barPercentage: 1.0,
          },
        ],
      },
    };
  }

  getBin() {
    this.getHisGraph();
  }
}
