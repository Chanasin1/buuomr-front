import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams } from '@angular/common/http';
// tslint:disable-next-line:import-blacklist
import { ApiService} from '../shared/services/api.service'


@Injectable()
export class graphService {


    constructor(
        private apiService: ApiService,
    ) { }

    ngOnIntit(){
        this.getStdStat();
    }

    getStdData() {
        return this.apiService.get(`/score`);
    }
    getStdScore
    () {
        return this.apiService.get(`/myscore`);
    }
    getStdStat(){
        return this.apiService.get(`/stats`);
    }
    getAllScore(){
        return this.apiService.get('/stdscore');
    }
    getClassScore(){
        return this.apiService.get('/classstats');
    }


}
