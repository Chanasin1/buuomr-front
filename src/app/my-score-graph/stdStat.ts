export class stdStat {
    id: number;
    stdId: number;
    name: String;
    score: number;
    minscore:number;
    maxscore: number;
    average: number;

    constructor(id,stdId,name,score,minscore,maxscore,average){
        this.id = id;
        this.stdId = stdId;
        this.name = name;
        this.score = score;
        this.minscore = minscore;
        this.maxscore = maxscore;
        this.average = average;
    }
}