import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { 
  MatButtonModule,
  MatIconModule, 
  MatSidenavModule, 
  MatFormFieldModule, 
  MatInputModule,
  MatToolbarModule,
  MatMenuModule,
  MatGridListModule,
  MatCheckboxModule,
  MatCardModule,
  MatSelectModule,
  MatTabsModule,
  MatListModule,
  MatDialogModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatAutocompleteModule,
  MatDatepickerModule,
 

} from '@angular/material';

const MaterialComponents = [
  MatButtonModule,
  MatIconModule,
  MatSidenavModule, 
  MatFormFieldModule, 
  MatInputModule,
  MatToolbarModule,
  MatMenuModule,
  MatGridListModule,
  MatCheckboxModule,
  MatCardModule,
  MatSelectModule,
  MatTabsModule,
  MatListModule,
  MatDialogModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatAutocompleteModule,
  MatDatepickerModule,
  
  
];

@NgModule({
  imports: [MaterialComponents],
  exports: [MaterialComponents]
 
})
export class MaterialModule { }

