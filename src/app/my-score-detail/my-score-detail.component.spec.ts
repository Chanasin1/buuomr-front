import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyScoreDetailComponent } from './my-score-detail.component';

describe('MyScoreDetailComponent', () => {
  let component: MyScoreDetailComponent;
  let fixture: ComponentFixture<MyScoreDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyScoreDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyScoreDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
