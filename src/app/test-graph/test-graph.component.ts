import { Component, OnInit } from '@angular/core';
import { Data } from '../models/data.model';
import * as d3 from 'd3';

@Component({
  selector: 'app-test-graph',
  templateUrl: './test-graph.component.html',
  styleUrls: ['./test-graph.component.scss']
})
export class TestGraphComponent implements OnInit {


  constructor() { }



  private testingdata = [
    10,
    5,
    5,
    12,
    10,
    1,
    10,
    12,
    13,
    12,
    3,
    15,
    12,
    10,
    11,
    10,
    10,
    10,
    10,
    11,
    20,
    11,
    12,
    10,
    10,
    11,
    10,
    16,
    15,
    12,
    13,
    10,
    17,
    14,
    10,
    12,
    5,
    10,
    6,
    6,
    16,
    11,
    7,
    12,
    10,
    12,
    15,
    14,
    20,
    12,
    11,
    5
  ];

  private svg: any;
  private margin = ({ top: 20, right: 20, bottom: 30, left: 40 });
  private width = 1000;
  private height = 800;
  private nbin = 15;


  ngOnInit(): void {
    this.createSvg();
    this.drawHist();
    this.updateChart();
  }
  private updateChart(): void {
    
  }

  private createSvg(): void {
    this.svg = d3.select("figure#hist")
    .append("svg")
      .attr("width", this.width + this.margin.left + this.margin.right)
      .attr("height", this.height + this.margin.top + this.margin.bottom)
    .append("g")
      .attr("transform",
        "translate(" + this.margin.left + "," + this.margin.top + ")");
  }

  private drawHist(): void {

    let tooltip = d3.select('body').append('div').attr('class', 'toolTip');

    let bins: any = d3.bin().thresholds(this.nbin)(this.testingdata)
    console.log(bins);

    let x = d3.scaleLinear().domain([bins[0].x0, bins[bins.length - 1].x1])
      .range([this.margin.left, this.width - this.margin.right])

    let yMaxDomain = Number(d3.max(bins, (d: any) => d.length));

    let y = d3.scaleLinear()
      .domain([0, yMaxDomain]).nice()
      .range([this.height - this.margin.bottom, this.margin.top])

    let xAxis = (g: any) => g
      .attr("transform", `translate(0,${this.height - this.margin.bottom})`)
      .call(d3.axisBottom(x).ticks(this.width / 80).tickSizeOuter(0))
      .call((g: any) => g.append("text")
        .attr("x", this.width - this.margin.right)
        .attr("y", -4)
        .attr("fill", "currentColor")
        .attr("font-weight", "bold")
        .attr("text-anchor", "end"));

    let yAxis = (g: any) => g
      .attr("transform", `translate(${this.margin.left},0)`)
      .call(d3.axisLeft(y).ticks(this.height / 40))
      .call((g: any) => g.select(".domain").remove())
      .call((g: any) => g.select(".tick:last-of-type text").clone()
        .attr("x", 4)
        .attr("text-anchor", "start")
        .attr("font-weight", "bold"));

    let myScore = 15; // change

    let filter = 10; //จำนวนที่ต้องการแบ่ง
    //console.log(this.testingdata.length);

    this.svg.append("circle").attr("cx", 200).attr("cy", 130).attr("r", 8).style("fill", "orange")
    this.svg.append("text").attr("x", 220).attr("y", 130).text("คะแนนที่ได้").style("font-size", "18px").attr("alignment-baseline", "middle")
    this.svg.append("circle").attr("cx", 200).attr("cy", 90).attr("r", 8).style("fill", "#EC5A40")
    this.svg.append("text").attr("x", 220).attr("y", 90).text("คะแนนทีไม่ผ่าน").style("font-size", "18px").attr("alignment-baseline", "middle")
    this.svg.append("circle").attr("cx", 200).attr("cy", 50).attr("r", 8).style("fill", "#69b3a2")
    this.svg.append("text").attr("x", 220).attr("y", 50).text("คะแนนที่ผ่าน").style("font-size", "18px").attr("alignment-baseline", "middle")

    this.svg.append("g")
      .selectAll("rect")
      .data(bins)
      .enter()
      .append("rect")
      .attr("x", (d: any) => x(d.x0) + 1)
      .attr("width", (d: any) => Math.max(0, x(d.x1) - x(d.x0) - 1))
      .attr("y", (d: any) => y(d.length))
      .attr("height", (d: any) => y(0) - y(d.length))
      //.style("fill", "#69b3a2")
      .transition()
      .duration(800)
      .style("fill", function (d: any) {
        if (filter > d.x0) {

          return "#EC5A40";
        } else {
          if (myScore >= d.x0 && myScore < d.x1) {
            return 'orange';
          }
          else { }
          return "#69b3a2";
        }
      });




    this.svg.append("g")
      .call(xAxis);

    this.svg.append("g")
      .call(yAxis);





  }


}