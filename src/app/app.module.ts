import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { HomeComponent } from './home/home.component';
import { CourseComponent } from './course/course.component';
import { ScoreComponent } from './score/score.component';
import { MyScoreComponent } from './my-score/my-score.component';
import { TeacherScoreComponent } from './teacher-score/teacher-score.component';
import { DownloadScoreComponent } from './download-score/download-score.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RootNavComponent } from './root-nav/root-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { TeacherScoreDetailComponent } from './teacher-score-detail/teacher-score-detail.component';
import { MyScoreDetailComponent } from './my-score-detail/my-score-detail.component';
import { MyScoreGraphComponent } from './my-score-graph/my-score-graph.component';
import { TeacherScoreGraphComponent } from './teacher-score-graph/teacher-score-graph.component';
import { ReportComponent } from './report/report.component';
import { DetailComponent } from './my-score/detail/detail.component';


import {AgGridModule } from 'ag-grid-angular';
import { HighchartsChartModule } from 'highcharts-angular';
import { TeacherExamDetailComponent } from './teacher-exam-detail/teacher-exam-detail.component';
import { HomeTeacherComponent } from './home-teacher/home-teacher.component';
import { TestGraphComponent } from './test-graph/test-graph.component';

import { ChartModule } from 'angular2-chartjs';

import { ApiService } from './shared/services/api.service'
import { graphService} from './my-score-graph/graphService'
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '',redirectTo: '/home', pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CourseComponent,
    ScoreComponent,
    MyScoreComponent,
    TeacherScoreComponent,
    DownloadScoreComponent,
    RootNavComponent,
    TeacherScoreDetailComponent,
    MyScoreDetailComponent,
    MyScoreGraphComponent,
    TeacherScoreGraphComponent,
    ReportComponent,
    DetailComponent,
    TeacherExamDetailComponent,
    HomeTeacherComponent,
    TestGraphComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AgGridModule.withComponents(),
    HighchartsChartModule,
    HttpClientModule,
    ChartModule,
    FormsModule
  ],
  providers: [
    ApiService,
    graphService],
  bootstrap: [AppComponent]
})
export class AppModule { }
