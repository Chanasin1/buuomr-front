import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  name: string;
  position: number;
  type: String;
  date: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Test', type: 'Final', date: '5/09/2563'},
  
];

@Component({
  selector: 'app-teacher-score',
  templateUrl: './teacher-score.component.html',
  styleUrls: ['./teacher-score.component.scss']
})
export class TeacherScoreComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'type', 'date','getdetails'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }


}
