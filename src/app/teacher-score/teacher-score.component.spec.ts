import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherScoreComponent } from './teacher-score.component';

describe('TeacherScoreComponent', () => {
  let component: TeacherScoreComponent;
  let fixture: ComponentFixture<TeacherScoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
