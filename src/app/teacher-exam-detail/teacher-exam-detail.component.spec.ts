import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherExamDetailComponent } from './teacher-exam-detail.component';

describe('TeacherExamDetailComponent', () => {
  let component: TeacherExamDetailComponent;
  let fixture: ComponentFixture<TeacherExamDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherExamDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherExamDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
