import { Component, OnInit } from '@angular/core';
import { graphService } from '../my-score-graph/graphService'

@Component({
  selector: 'app-teacher-exam-detail',
  templateUrl: './teacher-exam-detail.component.html',
  styleUrls: ['./teacher-exam-detail.component.scss']
})
export class TeacherExamDetailComponent implements OnInit {

  constructor(
    private graphservice: graphService,
  ) { }

  ngOnInit() {
  }

}
