import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadScoreComponent } from './download-score.component';

describe('DownloadScoreComponent', () => {
  let component: DownloadScoreComponent;
  let fixture: ComponentFixture<DownloadScoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
